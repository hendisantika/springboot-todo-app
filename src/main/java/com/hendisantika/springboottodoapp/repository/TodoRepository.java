package com.hendisantika.springboottodoapp.repository;

import com.hendisantika.springboottodoapp.domain.Todo;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-todo-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/12/19
 * Time: 14.02
 */
@Repository
public interface TodoRepository extends PagingAndSortingRepository<Todo, Long> {

}